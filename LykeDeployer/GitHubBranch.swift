//
//  GitHubBranch.swift
//  LykeDeployer
//
//  Created by Mohamed Fouad on 14/06/15.
//  Copyright © 2015 Mohamed Fouad. All rights reserved.
//

import Foundation

class GitHubBranch: NSObject {
    var name : String? = ""
    
    init(name : String?) {
        self.name = name
        super.init();
    }

}