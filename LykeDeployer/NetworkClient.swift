//
//  NetworkEngine.swift
//  LykeDeployer
//
//  Created by Mohamed Fouad on 14/06/15.
//  Copyright © 2015 Mohamed Fouad. All rights reserved.
//

import Foundation

struct Constants {
    static let gitHubAccessToken = "0466a0da86273e86bb39890e5964f263cd736353"
    static let gitHubRepo = "tajawal/ios"

    static let jenkinsBaseURL = "https://jenkins-live.tajawal.io:8443/"
    static let jenkinsUsername = "mohamed.fouad"
    static let jenkinsPassword = "e1d391ac036d723f4bcb852ffa20a5e6"

    static let loginString = String(format: "%@:%@", jenkinsUsername, jenkinsPassword)
    static let loginData = loginString.data(using: String.Encoding.utf8)!
    static let base64LoginString = loginData.base64EncodedString()

}


struct Project {
    let jenkinsJobName: String
    let jenkinsJobToken: String

    func url() -> URL? {
        return URL(string: Constants.jenkinsBaseURL + "job/" + jenkinsJobName + "/build" + "?token=" + jenkinsJobToken)
    }
}


enum TajawalProject {
    case tajawal
    case almosafer


    static func configuration(project: TajawalProject) -> Project {

        switch project {
        case tajawal:
            return Project(jenkinsJobName: "ci_ios_build_crashlytics_tajawal", jenkinsJobToken: "ci_ios_build_crashlytics_tajawal")
        case almosafer:
            return Project(jenkinsJobName: "ci_ios_build_crashlytics_almosafer", jenkinsJobToken: "ci_ios_build_crashlytics_almosafer")

        }
    }
}

class NetworkClient: NSObject {
    var urlSession = URLSession.shared
    let backgroundQueue: DispatchQueue = DispatchQueue(label: "com.mo7amedfouad.lykeDeployer.backgroundQueue", attributes: [])
    let gitHubURL = "https://api.github.com/repos/\(Constants.gitHubRepo)/branches?access_token=\(Constants.gitHubAccessToken)&per_page=100"


    func requestBranches(_ responseHandler: @escaping (_ error: NSError?, _ branches: Array<GitHubBranch>) -> ()) -> () {

        let url = URL(string: gitHubURL)
        let task = self.urlSession.dataTask(with: url!, completionHandler: { (data, response, error) -> Void in
            var branches: Array<GitHubBranch> = []
            do {
                let branchesJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! Array<NSDictionary>
                for branchJSON in branchesJSON {
                    let branch: GitHubBranch = GitHubBranch(name: branchJSON["name"] as? String);
                    branches.append(branch)
                }

                DispatchQueue.main.async(execute: { () -> Void in
                    responseHandler(nil, branches)
                })

            } catch {
                DispatchQueue.main.async(execute: { () -> Void in
                    responseHandler(nil, branches)
                })

            }
        })
        task.resume()
    }


    func deploy(_ branchName: String, project: Project, responseHandler: @escaping (_ error: NSError?) -> ()) -> () {
        self.getCrumb { crumb, crumbField in
            let jsonData = "{\"parameter\": [{\"name\":\"issue_branch\", \"value\":\"\(branchName)\"}]}"
            let params = "json=\(jsonData)"
            guard let url = project.url() else {
                return
            }
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue(crumb, forHTTPHeaderField: crumbField)
            request.setValue("Basic \(Constants.base64LoginString)", forHTTPHeaderField: "Authorization")
            request.httpBody = params.data(using: String.Encoding.utf8)
            print(request)
            let task = self.urlSession.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                DispatchQueue.main.async(execute: { () -> Void in
                    print(String(data: data!, encoding: .utf8) ?? "NO RESPONSE")
                    responseHandler(nil)

                })
            })
            task.resume()
        }
    }


    func getCrumb(responseHandler: @escaping (_ crumb: String, _ crumbField: String) -> ()) -> () {

        let url = URL(string: Constants.jenkinsBaseURL + "/crumbIssuer/api/json")
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Basic \(Constants.base64LoginString)", forHTTPHeaderField: "Authorization")

        let task = self.urlSession.dataTask(with: request as URLRequest) { (data, response, error) in
            do {
                print(String(data: data!, encoding: .utf8) ?? "NO RESPONSE")
                let branchesJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                let crumb = branchesJSON?["crumb"] as! String
                let crumbField = branchesJSON?["crumbRequestField"] as! String
                responseHandler(crumb, crumbField)
            } catch {
                DispatchQueue.main.async(execute: { () -> Void in
                    responseHandler("", "")
                })

            }
        }
        task.resume()

    }
}
