//
//  FirstViewController.swift
//  LykeDeployer
//
//  Created by Mohamed Fouad on 14/06/15.
//  Copyright © 2015 Mohamed Fouad. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    let client = NetworkClient()
    var branches: Array<GitHubBranch> = []
    var selectedBranch: GitHubBranch?
    let refreshControl = UIRefreshControl()
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Deployer"
        self.tableView.addSubview(self.refreshControl)
        self.refreshControl.addTarget(self, action: #selector(FirstViewController.fetch), for: UIControlEvents.valueChanged)

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.fetch()
        self.refreshControl.beginRefreshing()
    }

    func fetch() {
        self.client.requestBranches({ (error, branches) -> () in
            self.branches = branches
            self.refreshControl.endRefreshing()
            self.tableView.reloadData()
        })
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.branches.count
    }

    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "branch")!
        cell.textLabel?.text = self.branches[indexPath.row].name
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        self.selectedBranch = self.branches[indexPath.row]


        let alert = UIAlertController(title: "Building \(self.selectedBranch!.name!) for iOS", message: "choose app configuration", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Tajawal", style: UIAlertActionStyle.default, handler: alertHandler))
        alert.addAction(UIAlertAction(title: "Almosfer", style: UIAlertActionStyle.default, handler: alertHandler))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))

        self.present(alert, animated: true, completion: nil)
        self.tableView.deselectRow(at: self.tableView.indexPathForSelectedRow!, animated: true)


    }

    func alertHandler(_ alertAction: UIAlertAction!) {
        if (alertAction.style == UIAlertActionStyle.cancel) {
            return;
        }

        var project: Project
        if let title = alertAction.title, title == "Almosfer" {
            project = TajawalProject.configuration(project: .almosafer)
        } else {
            project = TajawalProject.configuration(project: .tajawal)
        }



        self.client.deploy(self.selectedBranch!.name!, project: project) { (error) -> () in
            print(error.debugDescription)
        }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
}

